import { useState, useContext } from 'react'

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

// App imports
import UserContext from '../UserContext';

const Login = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const {user, setUser} = useContext(UserContext)

    function login(e) {
        e.preventDefault();
        alert('You are now logged in.');
                
        setUser({email: email})

        setEmail('');
        setPassword('');
    } 

    return ( 
        <Container fluid>
        <h3>Login</h3>
        <Form onSubmit={login}>
            <Form.Group>
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
            </Form.Group>
            <Button variant="success" type="submit">Login</Button>
        </Form>
    </Container>
     );
}
 
export default Login;
