import { Link } from 'react-router-dom';


// Bootstrap Components
import Container from 'react-bootstrap/Container';

const NotFound = () => {
    return(
        <Container fluid>
            <h3>Page Not Found</h3>
            <p>Go back to the <Link to="/">homepage</Link>.</p>
        </Container>
    )
}
 
export default NotFound;
